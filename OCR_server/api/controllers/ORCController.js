'use strict';
var Tesseract = require('tesseract.js');
var Translator = require('translate');
var Multer = require('multer');
var Fs = require('fs');
var APIKey = require('../../assets/secret/GoogleAPIKey.js');
var Sharp = require('sharp');
var He = require('he');

var fileName = "";
var dirPath = 'assets/img/upload/';
var storage = Multer.diskStorage({
    destination: function (req, file, cb) {
        if (!Fs.existsSync(dirPath)) {
            var dir = Fs.mkdirSync(dirPath);
        }
        cb(null, dirPath + '/');
    },
    filename: function (req, file, cb) {
        var ext = file.originalname.substring(file.originalname.lastIndexOf("."));
        if(ext != '.jpg' && ext != '.png')
        {
            console.log("wrong file format : " + file.originalname);
            return;
        }

        fileName = Date.now() + ext;
        cb(null, fileName);
    }
});

exports.process = function(req,res){
    var da = new Date();
    var uploader = Multer({
        storage: storage
    }).single('Image');

    uploader(req,res, function(err) {
        if(err)
        {
            res.send({message:'err :' + err});
            throw err;
        } 

        let from = req.body.from;
        let to = req.body.to;

        Sharp(dirPath+fileName)
        .greyscale()
        .normalise()
        .threshold(128)
        .toBuffer()
        .then(function (data){
            ProcessImg(data,from,to).then(
                function (result){
                    res.json(result);
                }
            ).catch(err =>
                res.send({ error: true, message: 'error' + err, date:da})
            );
        })    
        .catch(err => console.log('failed to binarize : '+ err));
    });
}

async function ProcessImg(IMGpath,from = 'eng',to = 'vi'){
    var OriginalText;
    var _Error;
    let option = {
        lang: from,
    }

    await Tesseract.recognize(IMGpath,option)
    //.progress(function(p) { console.log('process',p)} )
    .then((res) => {console.log(res.text); OriginalText = res.text })
    .catch((err) => {console.log(err); _Error = err });

    let respond = {};
    if (!_Error)
    {
        if(!OriginalText)
            return ({error:true, message : "No Text Found"});

        respond.OriginalText = OriginalText;
        let Translated;
        await Translating(OriginalText,from,to)
        .then((res) => {console.log(res); Translated = res;})
        .catch((err) => {console.log(err); _Error = err});

        if(!_Error)
        {
            respond.TranslatedText = Translated;
            return respond;
        } 
    }
    return ({error:true, message : _Error});
}

async function Translating(string, from = 'en', to = 'vi'){
    console.log('start traslating from '+from+" to "+to);

    //cut the code since google use 2char code
    if (from.length > 2)
        from.substr(0,2);
    if (to.length > 2)
        to.substr(0,2);

    if (from === to)
        return string;

    var option = {from, to};
    option.engine = 'google';
    option.key = APIKey.key;
    var result;
    var _Error;
    await Translator(string, option)
    .then((res)     => {console.log(res); result = He.decode(res)})
    .catch((err)   => { console.log(err); _Error = err});
    
    if(result.error)
        return({error:true, message : _Error})

    return result;
}

//  tesseract   language                
//
// 'afr'	    Afrikaans               
// 'ara'	    Arabic                  
// 'aze'	    Azerbaijani
// 'bel'	    Belarusian
// 'ben'	    Bengali
// 'bul'	    Bulgarian
// 'cat'	    Catalan
// 'ces'	    Czech
// 'chi_sim'	Chinese
// 'chi_tra'	Traditional Chinese
// 'chr'	    Cherokee
// 'dan'	    Danish
// 'deu'	    German
// 'ell'	    Greek
// 'eng'	    English
// 'enm'	    English (Old)
// 'epo'	    Esperanto
// 'epo_alt'	Esperanto alternative
// 'equ'	    Math
// 'est'	    Estonian
// 'eus'	    Basque
// 'fas'	    Persian (Farsi)
// 'fin'	    Finnish
// 'fra'	    French
// 'frk'	    Frankish
// 'frm'	    French (Old)
// 'glg'	    Galician
// 'grc'	    Ancient Greek
// 'heb'	    Hebrew
// 'hin'	    Hindi
// 'hrv'	    Croatian
// 'hun'	    Hungarian
// 'ind'	    Indonesian
// 'isl'	    Icelandic
// 'ita'	    Italian
// 'ita_old'	Italian (Old)
// 'jpn'	    Japanese
// 'kan'	    Kannada
// 'kor'	    Korean
// 'lav'	    Latvian
// 'lit'	    Lithuanian
// 'mal'	    Malayalam
// 'mkd'	    Macedonian
// 'mlt'	    Maltese
// 'msa'	    Malay
// 'nld'	    Dutch
// 'nor'	    Norwegian
// 'pol'	    Polish
// 'por'	    Portuguese
// 'ron'	    Romanian
// 'rus'	    Russian
// 'slk'	    Slovakian
// 'slv'	    Slovenian
// 'spa'	    Spanish
// 'spa_old'	Old Spanish
// 'sqi'	    Albanian
// 'srp'	    Serbian (Latin)
// 'swa'	    Swahili
// 'swe'	    Swedish
// 'tam'	    Tamil
// 'tel'	    Telugu
// 'tgl'	    Tagalog
// 'tha'	    Thai
// 'tur'	    Turkish
// 'ukr'	    Ukrainian
// 'vie'	    Vietnamese