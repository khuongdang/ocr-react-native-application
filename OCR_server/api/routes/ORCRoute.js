'use strict';
module.exports = function(app){
    var ORC = require('../controllers/ORCController');

    //routing
    app.route('/process')
        .post(ORC.process);
}