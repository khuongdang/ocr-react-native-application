import React from 'react';
import { Image, View, StyleSheet, Dimensions, ActivityIndicator,Picker } from 'react-native';
import { Constants,ImagePicker } from 'expo';
import { Button } from 'react-native-elements';
var Config = require('../assets/config/config.js');

class PhotoScreen extends React.Component {
	state = {
			image: null,
			translated: "",
			rawSentence: "",
			dim : Dimensions.get('window'),
			working : false,
			from : "eng",
			to   : "vie"
  	};

	render() {
	    let { image,working } = this.state;
	    let width = this.state.dim.width;

	    return (
	      <View style = {styles.photoscreen}>
					< Button
						title="Take a picture"
						onPress={this.OpenCamera}
						buttonStyle={{
							backgroundColor:"#5588ff",
						}}
					/>
					< Button
						title="Pick an image from camera roll"
						onPress={this.PickImage}
						buttonStyle={{
							backgroundColor:"#5588ff",
						}}
					/>
					{image &&
						<Image source={{ uri: image }} style= {{ 
							width: width, 
							height:width 
						}} />}
					{image && 
						<View style={styles.container}>
							<View style={styles.picker_container}>
								<Picker
									selectedValue={this.state.from}
									style={styles.picker}
									onValueChange={(itemValue, itemIndex) => this.setState({from: itemValue})}>
										<Picker.Item label="English" value="eng" />
										<Picker.Item label="Vietnamese" value= "vie" />
										<Picker.Item label="Spain" value= "spa" />
										<Picker.Item label="French" value= "fra" />
								</Picker>

								<Picker
									style = {styles.picker}
									selectedValue={this.state.to}
									onValueChange={(itemValue, itemIndex) => this.setState({to: itemValue})}>
										<Picker.Item label="Vietnamese" value= "vie"/>
										<Picker.Item label="English" value= "eng"/>	
										<Picker.Item label="Spain" value= "spa" />		
										<Picker.Item label="French" value= "fra" />						
								</Picker>
							</View>
							<View>
								< Button
									style = {styles.button}
									title="Start tranlating"
									onPress={this.Translate}
									disabled={working == true}
									buttonStyle={{
										backgroundColor:"#5588ff"
									}}
								/>
							</View>
						</View>}
						{
							working == true && 
							<View style = {styles.loading}>
									<ActivityIndicator size = "large" color = "#999999"/>
							</View>
						}
	      </View>
	    );
	}

	OpenCamera = async () => {
		let result = await ImagePicker.launchCameraAsync({
			allowsEditing : true,
			aspect : [1,1],
			quality : 0.3,
		});

		if (!result.cancelled){
			this.setState({ image: result.uri, translatedText:"",originalText:""})
		}
	}

	PickImage = async () => {

	    let result = await ImagePicker.launchImageLibraryAsync({
			mediaTypes : "Images",
			allowsEditing : true,
			aspect : [1,1],
			quality : 0.3,
	    });

	    if (!result.cancelled) {
	      this.setState({ image: result.uri, translatedText:"",originalText:"" });
		}
	};

	Translate = async () => {
		await this.setState({working:true});
		var result = await this.SendImage().then(
			function(res) {
				this.props.navigation.navigate("Result",{
					translatedText : this.state.translatedText,
					originalText	: this.state.originalText,
				});
			}.bind(this)
		).catch(function(error){
			alert(error);
			throw error;
		}).finally(
			function(resOrError) {
				this.setState({working:false})
			}.bind(this)
		)

	}

	SendImage = async () => {
			let uri = this.state.image;
			let ApiURL = Config.PROCESS_URL;
			let uriParts = uri.split('.');
			let fileType = uriParts[uriParts.length - 1];
			let from = this.state.from;
			let to = this.state.to;
			
			const formData = new FormData();
			formData.append('Image', {
				uri,
				name: `photo.${fileType}`,
				type: `image/${fileType}`,
			});

			formData.append('from',from);
			formData.append('to',to);

			const options = {
				method: 'POST',
				timeout: 15000,
				body: formData,
				headers: {
					Accept: 'application/json',
					'Content-Type': 'multipart/form-data',
				},
			};

			console.log('sending');
			var result = await fetch(ApiURL, options)
			.then((Response)=>{return Response.json()})
			.then(function(Responsejson){
				this.setState({
					translatedText: Responsejson.TranslatedText, 
					originalText: Responsejson.OriginalText
				});
			}.bind(this)).catch(function(error){
				console.log(error);
				throw error;
			})
			console.log('done');
	}
}

const styles = StyleSheet.create({
	photoscreen: {
		flex: 1,
		alignItems: 'stretch',
		justifyContent: 'flex-start',
		backgroundColor: '#aaccff',
	},
  	button: {
		alignItems: 'stretch',
		justifyContent: 'center',   
		height: 50
	},
	loading: {
		position: 'absolute',
		left: 0,
		right: 0,
		top: 0,
		bottom: 0,
		alignItems: 'center',
		justifyContent: 'center',
		opacity:1,
		color:"#000",
	},
	picker: {
		height:100,
		width:200,
		alignItems: 'stretch',
		justifyContent: 'center',
	},
	picker_container:  {
		flexDirection:'row',
		justifyContent: 'flex-start',
	},
	container: {
		flex:1,
		alignItems: 'center',
		justifyContent: 'flex-end',
	}
});

export default PhotoScreen;