import React from 'react';
import { View,StyleSheet,TextInput,Text } from 'react-native';
import { Constants } from 'expo';

class ResultScreen extends React.Component{
    constructor(props){
		super(props);

        const { navigation } = props;
		this.state = {
            translatedText : navigation.getParam("translatedText",""),
            originalText : navigation.getParam("originalText",""),
        }
        console.log(this.state);
	}


    render(){
        return (
            <View style = {styles.resultScreen}>
                <Text style={styles.lable}>Original Text</Text>
                <TextInput 
                    multiline = {true}
                    style = {styles.textbox}
                    value = {this.state.originalText}
                    editable = {false}
                />
                <Text style={styles.lable}>Tranlated Text</Text>
                <TextInput 
                    multiline = {true}
                    style = {styles.textbox}
                    value = {this.state.translatedText}
                    editable = {false}
                />
            </View>
        );
    }

}

const styles = StyleSheet.create({
    resultScreen : {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        justifyContent: 'center',
        backgroundColor: '#aaccff',
        paddingTop: Constants.statusBarHeight,
    },
    lable : {
        alignContent: 'center',
        textAlign: 'center',
        fontSize   :20,
    },
    textbox : {
        flex: 1,
        margin:20,
        backgroundColor: '#bbddff',
        borderColor: 'gray',
        borderWidth: 2,
        borderRadius: 10
    }
});

export default ResultScreen;