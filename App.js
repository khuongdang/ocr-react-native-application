import React from 'react';
import { StyleSheet, View } from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import { Button } from 'react-native-elements';
import ResultScreen from './src/Result';
import PhotoScreen from './src/Photo';
import { Constants, Permissions } from 'expo';

class HomeScreen extends React.Component {
  	render() {
    	return (
    		<View style={styles.homescreen}>
			    	<Button
								large
			        	onPress={this.ToPhotoScreen.bind(this)}
								icon={{
									name:'book',
									type:'font-awesome'
								}}
								buttonStyle={{
									width:150,
									height:100,
									borderRadius:10,
									backgroundColor:"#5588ff"
								}}
								containerStyle={{
									justifyContent:'center'
								}}
								title = "START"
			        />
		    </View>
    	);
  	}

  	async ToPhotoScreen(){
  		let { status } = await Permissions.getAsync(Permissions.CAMERA_ROLL, Permissions.CAMERA);
  		if(status !== 'granted')
  		{
  			status = await Permissions.askAsync(Permissions.CAMERA_ROLL, Permissions.CAMERA);
  		}

  		if(status === 'granted')
				this.props.navigation.navigate('Photo');
			else
				alert('Hey! You heve not enabled selected permissions'+photo+camera);
  	}
}

const styles = StyleSheet.create({
  homescreen:{
		flex: 1,
		alignItems: 'center',
    justifyContent: 'center', 
  	flexDirection: 'column',
    backgroundColor: '#aaccff',
  }
});

const Application = createStackNavigator({
		Home : HomeScreen,
		Photo: PhotoScreen,
		Result: ResultScreen,
	},
	{
		initialRouteName : "Photo"
	}
);

const AppContainer = createAppContainer(Application);

export default class App extends React.Component {
  render() {
    return <AppContainer />;
  }
}